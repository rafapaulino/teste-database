﻿CREATE TABLE [dbo].[Usuarios]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] VARCHAR(255) NOT NULL, 
    [Email] VARCHAR(255) NOT NULL, 
    [Password] TEXT NOT NULL, 
    [Description] TEXT NULL, 
    [Photo] IMAGE NULL, 
    [DateInc] DATETIME NOT NULL
)
